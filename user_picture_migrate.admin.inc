<?php

/**
 * @file
 * Admin page callbacks for the user_picture_migrate module.
 */

/**
 * Implements hook_form().
 */
function user_picture_migrate_settings_form() {
  $total = _user_picture_migrate_total_pictures();

  $form['target'] = array(
    '#type' => 'fieldset',
    '#title' => t('Migration target field'),
    '#description' => t('Submitting this form will trigger a batch operation to copy the user picture to the selected field.'),
  );
  $form['target']['total'] = array(
    '#markup' => t('Total user pictures detected: @total.', array('@total' => $total)),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $options = _user_picture_migrate_get_available_target_fields();
  if (!empty($options)) {
    $form['target']['target_field'] = array(
      '#type' => 'select',
      '#title' => t('Target field'),
      '#options' => _user_picture_migrate_get_available_target_fields(),
      '#description' => t('Select the target image field for the migration.'),
      '#required' => TRUE,
    );
    $form['target']['overwrite_data'] = array(
      '#type' => 'checkbox',
      '#title' => t('Overwrite existing data'),
      '#description' => t('Override files in the target field with the current user picture.'),
    );
    $form['target']['delete_user_pictures'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete user pictures'),
      '#description' => t('Delete the user pictures when the migration has finished.'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Start migration'),
    );
    if (!$total) {
      $form['submit']['#disabled'] = TRUE;
    }
  }
  else {
    $form['target']['target_field'] = array(
      '#type' => 'item',
      '#title' => t('Target field'),
      '#markup' => t('No image fields available. <a href="@account_fields_url">Add an image field first</a>.', array('@account_fields_url' => '/admin/config/people/accounts/fields')),
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function user_picture_migrate_settings_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  $options = $form_state['values'];

  $batch = user_picture_migrate_batch($options);
  batch_set($batch);
}

/**
 * Helper function to generate a batch operation instruction.
 *
 * @param array $options
 *   The configuration options from
 *
 * @return array
 *   A batch instruction
 */
function user_picture_migrate_batch($options) {
  $operations = array();

  $total = _user_picture_migrate_total_pictures();
  $batches = (int) ($total / 20) + 1;
  for ($i = 0; $i < $batches; $i++) {
    // Each operation is an array consisting of
    // - the function to call.
    // - An array of arguments to that function.
    $operations[] = array('user_picture_migrate_batch_operation', array($options));
  }

  $batch = array(
    'operations' => $operations,
    'title' => t('Migrating user pictures'),
    'file' => drupal_get_path('module', 'user_picture_migrate') . '/user_picture_migrate.admin.inc',
    'init_message' => t('Starting user picture migration.'),
    'progress_message' => t('Processed batch @current out of @total.'),
    'error_message' => t('User picture migration has encountered an error.'),
    'finished' => 'user_picture_migrate_batch_finished',
  );

  return $batch;
}

/**
 * The batch operation.
 *
 * @param array $options
 *   An array of configuration options
 * @param array $context
 *   Information on the current operation
 */
function user_picture_migrate_batch_operation($options, &$context) {
  $target_field = $options['target_field'];
  // Initialize total values to process.
  if (!isset($context['sandbox']['total'])) {
    $limit = 20;
    $context['results'] = 0;
    $context['sandbox']['total'] = _user_picture_migrate_total_pictures();
    $context['sandbox']['current_user'] = 0;
    $context['sandbox']['processed'] = 0;
  }

  if ($context['sandbox']['total']) {
    // Retrieve next rows to migrate.
    $rows = db_select('users', 'u')
      ->fields('u', array('uid', 'picture'))
      ->orderBy('uid', 'ASC')
      ->condition('uid', $context['sandbox']['current_user'], '>')
      ->condition('picture', 0, '>')
      ->extend('PagerDefault')
      ->limit($limit)
      ->execute()
      ->fetchAllKeyed();

    foreach ($rows as $uid => $fid) {
      // Check for existing field_data
      $existing_fid = db_select('field_data_' . $target_field, 'f')
        ->fields('f', array($target_field . '_fid'))
        ->condition('entity_id', $uid, '=')
        ->condition('bundle', 'user', '=')
        ->condition('deleted', 0, '=')
        ->execute()
        ->fetchField();

      // Overwrite existing field with the user picture.
      if ($existing_fid && $options['overwrite_data'] && $fid != $existing_fid) {
        db_update('field_data_' . $target_field)
          ->condition('entity_id', $uid, '=')
          ->condition($target_field . '_fid', $existing_fid, '=')
          ->fields(array(
            $target_field . '_fid' => $fid,
          ))
          ->execute();
        db_update('field_revision_' . $target_field)
          ->condition('entity_id', $uid, '=')
          ->condition($target_field . '_fid', $existing_fid, '=')
          ->fields(array(
            $target_field . '_fid' => $fid,
          ))
          ->execute();
        // Reduce file usage from existing data.
        db_update('file_usage')
          ->condition('fid', $existing_fid)
          ->condition('module', 'file')
          ->condition('type', 'user')
          ->condition('id', $uid)
          ->expression('count', 'count - 1')
          ->execute();

        // It is now safe to delete the old user picture.
        if ($options['delete_user_pictures']) {
          $file = file_load($existing_fid);
          file_delete($file);
        }
      }
      elseif ($existing_fid && $fid != $existing_fid && !$options['overwrite_data'] && $options['delete_user_pictures']) {
        // Keep current field data and delete the user picture.
        $file = file_load($fid);
        file_delete($file);
      }
      elseif ($existing_fid && $fid == $existing_fid && !$options['overwrite_data'] && $options['delete_user_pictures']) {
        // Delete user picture file usage.
        db_delete('file_usage')
          ->condition('fid', $fid)
          ->condition('module', 'user')
          ->condition('type', 'user')
          ->condition('id', $uid)
          ->execute();
      }
      // Insert field data with the user picture.
      elseif (!$existing_fid) {
        // Add a row to the field data and revision tables.
        db_insert('field_data_' . $target_field)
          ->fields(array(
            'entity_type' => 'user',
            'bundle' => 'user',
            'entity_id' => $uid,
            'revision_id' => $uid,
            'language' => LANGUAGE_NONE,
            'delta' => 0,
            $target_field . '_fid' => $fid,
          ))
          ->execute();
        db_insert('field_revision_' . $target_field)
          ->fields(array(
            'entity_type' => 'user',
            'bundle' => 'user',
            'entity_id' => $uid,
            'revision_id' => $uid,
            'language' => LANGUAGE_NONE,
            'delta' => 0,
            $target_field . '_fid' => $fid,
          ))
          ->execute();

        if ($options['delete_user_pictures']) {
          // Move file usage from user to file module.
          db_update('file_usage')
            ->condition('fid', $fid)
            ->condition('module', 'user')
            ->condition('type', 'user')
            ->condition('id', $uid)
            ->fields(array(
              'module' => 'file',
            ))
            ->execute();
        }
        else {
          // The file and user module use the file.
          db_insert('file_usage')
            ->fields(array(
              'fid' => $fid,
              'module' => 'file',
              'type' => 'user',
              'id' => $uid,
              'count' => 1,
            ))
            ->execute();
        }
      }
      elseif ($options['delete_user_pictures']) {
        db_delete('file_usage')
          ->condition('fid', $fid)
          ->condition('module', 'user')
          ->condition('type', 'user')
          ->condition('id', $uid)
          ->execute();
      }

      // Update the sandbox with the current uid.
      $context['sandbox']['current_user'] = $uid;
    }

    if ($options['delete_user_pictures']) {
      // Remove user picture file references.
      db_update('users')
        ->fields(array(
          'picture' => 0,
        ))
        ->condition('uid', array_keys($rows))
        ->execute();
    }

    // Report status.
    $context['results'] += count($rows);
  }

  $context['sandbox']['#finished'] = $context['sandbox']['total'] ? $context['sandbox']['processed'] / $context['sandbox']['total'] : 1;
}

/**
 * Batch finished callback for user_picture_migrate_batch().
 *
 * @param bool $success
 *   The result of the batch operation
 * @param array $results
 *   The processed accounts
 * @param array $operations
 *   An array of batch operations
 */
function user_picture_migrate_batch_finished($success, $results, $operations) {
  if ($success) {
    cache_clear_all('*', 'cache_field', TRUE);
    drupal_set_message(format_plural($results, 'Migrated 1 user picture.', 'Migrated @count user pictures.'));
    drupal_set_message(t('Disable user pictures in the <a href="@account_settings_url">Account settings</a> when you no longer need them.', array(
      '@account_settings_url' => '/admin/config/people/accounts',
    )));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments: @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    )));
  }
}

/**
 * Helper function to generate a list of available target fields.
 *
 * @param string $entity_type
 *   The entity type to search for available fields
 *
 * @return array
 *   An array for fields of type 'image'.
 */
function _user_picture_migrate_get_available_target_fields($entity_type = 'user') {
  $targets = array();

  $fields_info = field_info_instances($entity_type);
  foreach ($fields_info[$entity_type] as $field_name => $value) {
    $field_info = field_info_field($field_name);
    if ($field_info['type'] == 'image') {
      $targets[$field_name] = t('@field_label (@field_name)', array('@field_label' => $fields_info[$entity_type][$field_name]['label'], '@field_name' => $field_name));
    }
  }

  return $targets;
}

/**
 * Helper function to count the users with a user picture.
 *
 * @return int
 *   The total amount of users with a user picture.
 */
function _user_picture_migrate_total_pictures() {
  $total = (int) db_query('SELECT COUNT(picture) FROM {users} WHERE picture > 0')->fetchField();

  return $total;
}
