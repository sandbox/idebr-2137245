
-- SUMMARY --

User Picture Migrate copies Drupal user pictures to an Image field of your
choice. This enables you to leverage the power of Field API, for example
revisions, internationalization. On top of that, various contributed modules
allow interaction with Image fields, such as cropping.

For a full description of the module, visit the project page:
  http://drupal.org/project/user_picture_migrate

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/user_picture_migrate


-- REQUIREMENTS --

* The Image module.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Select an Image field that is already available under Account settings.

* Optionally overwrite existing field data or let user pictures be deleted after
  the migration


-- FAQ --

Q: Can I do a use picture migrate multiple times?

A: Yes, you can choose to copy the pictures over first, fix your displays and
   then delete the old user pictures.


Q: What happens if a user uploads a different image in the mean time?

A: Overwriting existing data is optional, so you can choose to only copy over
   user pictures for users who did not upload an image in the Image field.

   During the migration, it is recommended to enable Maintenance mode on your
   site to prevent data conflicts.
